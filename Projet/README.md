## Projet Statistique

 **Auteurs** :
	- Thibaut Masselin
	- Corentin Dervieux

 **Sujet : Les jeux olympique d'hiver**

 Jeu de données utilisée :
 - [Les jeux olympique d'hiver](./winter_olympics_medals.csv) prevenant de data.gouv.fr
 
 Adresse : "https://raw.githubusercontent.com/askmedia/datalogue/master/olympics/winter_olympics_medals.csv"

 **Questions :**
  - Est ce que le fait d'organiser les jeux olympique augmente le nombre de medaille gagner du pays organisateur ?
  - Ou est ce que cela augmente la qualiter des médailles?
  - Certains sports sont-t-ils plus influencée par le fait d'organiser les jeux?

 Examiner l'évolution de sports et de spécialité organiser lors des jeux.


 